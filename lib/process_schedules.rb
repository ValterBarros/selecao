require 'active_support/time'
require_relative './lecture'

class ProcessSchedules
	attr_accessor :tracks_times_processed, :time, :processed_hours
	
	def initialize
		@tracks_times_processed = ""
		@processed_hours = Lecture.new.get_proposals_txt
		start_time_track
	end

	def unified_method_schedules(hours, is_morning)
		start_time_track if is_morning
		times_only = all_schedules
		times_only.each do |hour| 
	  	total_needed = hours / hour
	  	while total_needed > 0
				search_result = search(hour, total_needed)
				if !search_result.nil?
					current_decrement = total_needed * hour
					if !is_morning && (current_decrement == hours)
						total_needed -= 1
					else
						search_result_with_hours = calc_time(search_result, hour)
						hours -= current_decrement
						pass_content_tracks(search_result_with_hours)
						drop_processed_hours(hour, total_needed)
						total_needed = 0
					end
				end
				total_needed -= 1
			end
		end
		set_lunch_or_networking(hours, is_morning)
	end

	def set_name_track(name_track)
		@tracks_times_processed << name_track
	end

	def calc_tracks_number
		track_time = 420
		final_result = 0
		@processed_hours.each do |x,y| 
			size = @processed_hours[x].size
			final_result += size * x
		end

		track_number = final_result.to_f / track_time
		track_number = track_number.round
	end

	private
	def set_lunch_or_networking(hours,is_morning)
		@tracks_times_processed << if is_morning && (hours == 0)
		 	"12:00 Almoço\n"
		else
			"#{@time.strftime('%H:%M')} Evento de Networking\n"
		end
	end

	def start_time_track
		@time = Time.now.change({ hour:9, min:0, sec:0 })
	end

	def calc_time(search_result, hour)
		set_lunch_hour
		search_result = search_result.map do |t| 
			final_time = "#{@time.strftime('%H:%M')} #{t}"
			@time += hour * 60
			final_time
		end
		search_result
	end
	
	def set_lunch_hour
		if @time.strftime('%H:%M') == "12:00"
			@time = Time.now.change({ hour: 13, min: 0, sec: 0 })
		end
	end

	def all_schedules
		@processed_hours.map{|x,y| x}.sort.reverse
	end

	def search(hour, range)
		return nil if range > @processed_hours[hour].size
		@processed_hours[hour][0..(range-1)].map{ |t| "#{t} #{hour}min"}
	end

	def drop_processed_hours(hour, range)
		@processed_hours[hour] = @processed_hours[hour].drop(range)
	end

	def pass_content_tracks(search_result)
		search_result.map{ |t| @tracks_times_processed << "#{t} \n" }
	end
end