class Lecture
	attr_accessor :processed_hours
	
	def initialize
		@processed_hours = {}
	end

	def get_proposals_txt
		File.open("./data/proposals.txt") do |file|
			file.each_line do |line|
				name_proposal = get_name(line)
				time_proposal = get_time(line)
				@processed_hours[time_proposal] ||= []
				@processed_hours[time_proposal] << name_proposal
			end
		end
		@processed_hours
	end

	def get_name(text)
		text = text.split(/(\d+|lightning)/)
		text.first.strip
	end

	def get_time(text)
		exp = /(\d+|lightning)/.match(text)
		if exp[0] == "lightning"
			5
		else
			exp[0].to_i
		end
	end
end
