require_relative './process_schedules'

class Conference
  attr_accessor :process_schedules
  
  def initialize
  	@process_schedules = ProcessSchedules.new
  end
  
  def final_output
  	@process_schedules.tracks_times_processed
  end
  
  def make_track
		number_tracks = @process_schedules.calc_tracks_number
		schedules_number = 2
		names_track = ("A".."Z").to_a
		for i in 0..(number_tracks-1)
			@process_schedules.set_name_track("Track: #{names_track[i]} \n")
			for j in 0..(schedules_number-1)
				if j == 0
					hours = 180
					@process_schedules.unified_method_schedules(hours, true)
				else
					hours = 240
					@process_schedules.unified_method_schedules(hours, false)
				end
			end	
		end
	end
end
