require 'lecture'

RSpec.describe Lecture, "Input lectures from txt" do
	it "Pass a text and expect return only the lecture title" do
		text = "Diminuindo tempo de execução de testes em aplicações Rails enterprise 60min"
		lec = Lecture.new
		expect(lec.get_name(text)).to eql("Diminuindo tempo de execução de testes em aplicações Rails enterprise")
	end

	it "Pass a text and expect return only the lecture time" do
		lec = Lecture.new
		expect(lec.get_time("Diminuindo tempo de execução de testes em aplicações Rails enterprise 60min")).to eq(60)
		expect(lec.get_time("Rails para usuários de Django lightning")).to eq(5)
	end
end
