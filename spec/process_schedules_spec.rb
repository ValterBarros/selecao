require 'process_schedules'

RSpec.describe ProcessSchedules, "Schedules ordination" do
	context "Process the schedules" do
		it "Tracks_times_processed is null?" do
			process = ProcessSchedules.new
			process.unified_method_schedules(180, true)
			expect(process.tracks_times_processed != "").to be (true)
		end

		it "When the processschedules is initializado the time track is started" do
			process = ProcessSchedules.new
			expect(process.time.strftime('%H:%M')).to eq("09:00")
		end
	end
end